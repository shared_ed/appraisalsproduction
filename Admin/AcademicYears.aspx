﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Site.Master" AutoEventWireup="true" CodeBehind="AcademicYears.aspx.cs" Inherits="Appraisals.Admin.AcademicYearsWebForm" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="container">
          <div class="jumbotron">
            <h2>Academic Year Maintenenance</h2>      
            <p>Please contact <a href="mailto:david.slater@shared-ed.co.uk;russ.davidson@shared-ed.co.uk">MIS</a> to have another Academic Year added to the HR Appraisals application. Thank you.</p>
          </div>
    </div>
</asp:Content>
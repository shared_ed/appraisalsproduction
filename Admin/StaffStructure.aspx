﻿<%@ Page Title="" Language="C#" MasterPageFile="Site.Master" AutoEventWireup="true" CodeBehind="StaffStructure.aspx.cs" Inherits="Appraisals.Admin.StaffStructureWebForm" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="well well-sm" style="text-align:center">
        <h1 id="objTypeTitle" runat="server">Maintain Staff Structure - Add New</h1>
    </div>

    <div style="padding-bottom:15px;"><a class="btn btn-default" href="StaffStructureChange">Change Existing &raquo;</a></div>

    <div id="messages" class="alert alert-success alert-dismissible" runat="server" visible="false">
    </div>
    <div id="errors" class="alert alert-danger alert-dismissible" runat="server" visible="false">
    </div>
    
    <div class="panel panel-default">
        <div class="panel-heading">Map managers to the staff members they will appraise.</div>
        <div class="panel-body">

        <div class="row"  runat="server">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="managerID">'Staff ID' of manager doing appraisal</label>
                    <input type="text"  class="form-control" id="managerID" runat="server">
                </div>
            </div>

        </div>

         <div class="row"  runat="server">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="StaffID">'Staff ID' of colleague being appraised</label>
                    <input type="text"  class="form-control" id="StaffID" runat="server">
                </div>
            </div>
        </div>

        <div class="row"  runat="server">
             <div class="col-md-12">
                <div class="form-group">
                    <label for="startDate">Date From <i>(format: dd/mm/yyyy)</i></label>
                    <input type="text"  class="form-control" id="startDate" runat="server">
                </div>
            </div>
 
        </div>

        <div class="row"  runat="server" >
             <div class="col-md-3" >
                <div class="form-group" >
                    <label for="endDate">Date To <i>(format: dd/mm/yyyy)</i>. <br />(If blank then expiry date will not be set)</label>
                    <input type="text"  class="form-control" id="endDate" runat="server">
                </div>
            </div>
        </div>

      
        <div id="verifySection" runat="server">
            <div class="row" runat="server">
                <div class="col-md-12">
                    <div class="form-group">
                        Please click the 'Verify' button so that you are able to view & check the details of the Staff members specified above.
                    </div>
                </div>
            </div>

            <div class="row" runat="server">
                 <div class="col-md-12">
                    <div class="form-group">
                        <asp:Button ID="btnVerify" runat="server" Text="Verify" class="btn btn-default" 
                            CommandName="Verify"
                            CommandArgument="None"
                            OnCommand="CommandBtn_Click"/>
                    </div>
                </div>
            </div>
        </div>

        <div id="staffDetailsSection" visible="false" runat="server">

            <div class="row"  runat="server">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="managerDetails">Manager's details</label>
                        <textarea class="form-control" style="min-width: 86%" rows="5" id="managerDetails" runat="server" readonly></textarea>
                    </div>
                </div>
            </div>

             <div class="row"   runat="server">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="staffDetails">Appraised colleague's details</label>
                        <textarea class="form-control" style="min-width: 86%" rows="5" id="staffDetails" runat="server" readonly></textarea>
                    </div>
                </div>
            </div>

            <div class="row" runat="server">
                <div class="col-md-12">
                    <div class="form-group">
                        If the staff details above are correct then click the 'Add' button to save the new 'Staff Structure' mapping. Click the 'Clear' button if you want to reset all form values.
                    </div>
                </div>
            </div>

            <div class="row" id="btnAddRow"  runat="server">
                <div class="col-md-2">
                    <asp:Button ID="btnAdd" runat="server" Text="Add" class="btn btn-default" 
                        CommandName="Add"
                        CommandArgument="None"
                        OnCommand="CommandBtn_Click"/>
                </div>
                <div class="col-md-10">
                    <asp:Button ID="Button1" runat="server" Text="Clear" class="btn btn-default" 
                        CommandName="Clear"
                        CommandArgument="None"
                        OnCommand="CommandBtn_Click"/>
                </div>
            </div>
        </div>
    </div>

    <asp:HiddenField ID="hiddenMgrID" runat="server" />
    <asp:HiddenField ID="hiddenStaffID" runat="server" />
    <asp:HiddenField ID="hiddenStartDate" runat="server" />
    <asp:HiddenField ID="hiddenEndDate" runat="server" />
</asp:Content>

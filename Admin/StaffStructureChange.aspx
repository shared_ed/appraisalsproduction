﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Site.Master" AutoEventWireup="true" CodeBehind="StaffStructureChange.aspx.cs" Inherits="Appraisals.Admin.StaffStructureChangeWebForm" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="well well-sm" style="text-align:center">
        <h1 id="objTypeTitle" runat="server">Maintain Staff Structure - Change Existing</h1>
    </div>

    <div style="padding-bottom:15px;"><a class="btn btn-default" href="StaffStructure">&laquo; Add New </a></div>

    <div id="messages" class="alert alert-success alert-dismissible" runat="server" visible="false">
    </div>
    <div id="errors" class="alert alert-danger alert-dismissible" runat="server" visible="false">
    </div>
    
    <div class="panel panel-default">
        <div class="panel-heading">Change the period(s) that an existing Staff Structure link is active.</div>
        <div class="panel-body">

        <div class="row"  runat="server">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="managerID">'Staff ID' of manager doing appraisal</label>
                    <input type="text"  class="form-control" id="managerID" runat="server">
                </div>
            </div>

        </div>

         <div class="row"  runat="server">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="StaffID">'Staff ID' of colleague being appraised</label>
                    <input type="text"  class="form-control" id="StaffID" runat="server">
                </div>
            </div>
        </div>

       

        <div id="verifySection" runat="server">
            <div class="row" runat="server">
                <div class="col-md-12">
                    <div class="form-group">
                        Please click the 'Verify' button so that you are able to view & check the details of the Staff members specified above.
                    </div>
                </div>
            </div>

            <div class="row" runat="server">
                 <div class="col-md-12">
                    <div class="form-group">
                        <asp:Button ID="btnVerify" runat="server" Text="Verify" class="btn btn-default" 
                            CommandName="Verify"
                            CommandArgument="None"
                            OnCommand="CommandBtn_Click"/>
                    </div>
                </div>
            </div>
        </div>

        <div id="staffDetailsSection" visible="false" runat="server">

                <div class="row"  runat="server">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="managerDetails">Manager's details</label>
                            <textarea class="form-control" style="min-width: 86%" rows="5" id="managerDetails" runat="server" readonly></textarea>
                        </div>
                    </div>
                </div>

                 <div class="row"   runat="server">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="staffDetails">Appraised colleague's details</label>
                            <textarea class="form-control" style="min-width: 86%" rows="5" id="staffDetails" runat="server" readonly></textarea>
                        </div>
                    </div>
                </div>
        </div>

         <div runat="server" id="resetSection" visible="false">
            <div class="row" runat="server" >
                <div class="col-md-12">
                    <div class="form-group">
                        Click the 'Reset' button if you want to reset all form values and try again.
                    </div>
                </div>
            </div>

            <div class="row" runat="server" id="Div1">
                <div class="col-md-12">
                        <asp:Button ID="Button2" runat="server" Text="Reset" class="btn btn-default" 
                            CommandName="Clear"
                            CommandArgument="None"
                            OnCommand="CommandBtn_Click"/>
                </div>
            </div>
        </div>

        <div id="datesSection" visible="false" runat="server">
             <div class="row" runat="server" style="padding-top:20px;">
                <div class="col-md-12">
                    <div class="form-group">
                        If the staff details above are correct then please amend the date-ranges below as required. Note that a blank 'Date To' means that the specified range has no expiry date.  
                    </div>
                </div>
            </div>

            <div class="row">
                    <div class="col-md-6">
                      <table class="table">
                        <thead>
                            <tr>
                                <th>Date From</th>
                                <th>Date To</th>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <asp:Repeater ID="dateRanges" runat="server">
                                <ItemTemplate>
                                    <tr>
                                        <td><asp:TextBox class="form-control" Columns="200"  ID="fromDateNew" runat="server" Text='<%# Eval("DateFromNew") %>' /></td>
                                        <td><asp:TextBox  class="form-control" ID="toDateNew" runat="server" Text='<%# Eval("DateToNew") %>' /></td>
                                        <td><asp:TextBox class="form-control" Style="display:none;"  ID="fromDateOld" runat="server" Text='<%# Eval("DateFromOld") %>'  /></td>
                                        <td><asp:TextBox class="form-control" Style="display:none;"  ID="toDateOld" runat="server" Text='<%# Eval("DateToOld") %>'  /></td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </tbody>
                    </table>
                </div>
            </div>

            <%--<div class="row"  runat="server">
             <div class="col-md-12">
                <div class="form-group">
                    <label for="startDate">Date From <i>(format: dd/mm/yyyy)</i></label>
                    <input type="text"  class="form-control" id="startDate" runat="server">
                </div>
            </div>
         </div>

            <div class="row"  runat="server">
                 <div class="col-md-12">
                    <div class="form-group">
                        <label for="endDate">Date To<i>(format: dd/mm/yyyy)</i></label>
                        <input type="text"  class="form-control" id="endDate" runat="server">
                    </div>
                </div>
            </div>--%>

            <div class="row" runat="server">
                <div class="col-md-12">
                    <div class="form-group">
                        Click the 'Save' button to save your 'Staff Structure' changes.
                        <br />
                        Click the 'Reset' button if you want to reset all form values.
                    </div>
                </div>
            </div>

            <div class="row" id="btnAddRow"  runat="server">
                <div class="col-md-2">
                    <asp:Button ID="btnAdd" runat="server" Text="Save" class="btn btn-default" 
                        CommandName="Add"
                        CommandArgument="None"
                        OnCommand="CommandBtn_Click"/>
                </div>
                <div class="col-md-10">
                    <asp:Button ID="Button1" runat="server" Text="Reset" class="btn btn-default" 
                        CommandName="Clear"
                        CommandArgument="None"
                        OnCommand="CommandBtn_Click"/>
                </div>
            </div>
        </div>
    </div>

    <asp:HiddenField ID="hiddenMgrID" runat="server" />
    <asp:HiddenField ID="hiddenStaffID" runat="server" />
    <asp:HiddenField ID="hiddenStartDate" runat="server" />
    <asp:HiddenField ID="hiddenEndDate" runat="server" />
</asp:Content>

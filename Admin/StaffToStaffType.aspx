﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Site.Master" AutoEventWireup="true" CodeBehind="StaffToStaffType.aspx.cs" Inherits="Appraisals.Admin.StaffToStaffTypeWebForm" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="well well-sm" style="text-align:center">
        <h1 id="objTypeTitle" runat="server">Assign Staff to a Staff Type</h1>
    </div>

    <div id="messages" class="alert alert-success alert-dismissible" runat="server" visible="false"/>
    <div id="errors" class="alert alert-danger alert-dismissible" runat="server" visible="false"/>
    
    <div class="panel panel-default">
        <div class="panel-heading">Add or amend a 'Staff' to 'Staff Type' mapping.</div>
        <div class="panel-body">

            <div class="row"  runat="server">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="staffID">Please enter a 'Staff ID' <i>(Format: integer)</i></label>
                        <input type="text"  class="form-control" id="staffID" runat="server">
                    </div>
                </div>

            </div>

             <div class="row" runat="server">
                <div class="col-md-2">
                    <asp:Button ID="btnSearch" runat="server" Text="Search" class="btn btn-default" 
                        CommandName="Search"
                        CommandArgument="None"
                        OnCommand="CommandBtn_Click"/>
                </div>
             </div>

            <div id="staffTypeRow" visible="false" runat="server" style="margin-top:30px;">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group" runat="server" id="adviceText"/>
                    </div>
                </div>

                <div class="row"  runat="server">
                        <div class="col-md-12">
                             <div class="form-group">
                                <label for="staffTypeDropDown">Staff Type</label>
                                <asp:DropDownList ID="staffTypeDropDown" runat="server" CssClass="form-control"/>
                            </div>
                        </div>
                </div>

                <div class="row" runat="server">
                    <div class="col-md-1">
                        <asp:Button ID="btnSave" runat="server" Text="Save" class="btn btn-default" 
                            CommandName="Save"
                            CommandArgument="None"
                            OnCommand="CommandBtn_Click"/>
                    </div>
                    <div class="col-md-3">
                        <asp:Button ID="btnRest" runat="server" Text="Reset" class="btn btn-default" 
                            CommandName="Reset"
                            CommandArgument="None"
                            OnCommand="CommandBtn_Click"/>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <asp:HiddenField ID="hiddenStaffTypeID" runat="server" />
    <asp:HiddenField ID="hiddenStaffTypeDescription" runat="server" />
    <asp:HiddenField ID="HiddenIsInsert" runat="server" />

</asp:Content>
﻿<%@ Page Title="" Language="C#" MasterPageFile="Site.Master" AutoEventWireup="true" CodeBehind="StaffType.aspx.cs" Inherits="Appraisals.Admin.StaffTypeWebForm" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="container">
          <div class="jumbotron">
            <h2>Staff Type Maintenenance</h2>      
            <p>Please contact <a href="mailto:david.slater@shared-ed.co.uk;russ.davidson@shared-ed.co.uk">MIS</a> if you need to have a 'Staff Type' changed, added or deleted. Thank you.</p>
          </div>
    </div>
</asp:Content>
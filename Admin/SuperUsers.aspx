﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Site.Master" AutoEventWireup="true" CodeBehind="SuperUsers.aspx.cs" Inherits="Appraisals.Admin.SuperUsersWebForm" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="well well-sm" style="text-align:center">
        <h1 id="objTypeTitle" runat="server">Maintain Super Users</h1>
    </div>

    <div id="messages" class="alert alert-success alert-dismissible" runat="server" visible="false">
    </div>
    <div id="errors" class="alert alert-danger alert-dismissible" runat="server" visible="false">
    </div>
    
     <div class="panel panel-default">
        <div class="panel-heading"><b>Add</b> a <i>new</i> Super User.</div>
        <div class="panel-body">

           
    
            <div class="row"  runat="server">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="username">Provide a Username & click 'Add' to create a new 'Super User'</label>
                        <input type="text"  class="form-control" id="username" runat="server">
                    </div>
                </div>
            </div>

            <div class="row" runat="server" id="AddInitialRow">
                <div class="col-md-2">
                    <div class="form-group">
                        <asp:Button ID="btnAddInitial" runat="server" Text="Add" class="btn btn-default" 
                            CommandName="AddInitial"
                            CommandArgument="None"
                            OnCommand="CommandBtn_Click"/>
                    </div>
                </div>
            </div>

            <div id="functionsAddRow" runat="server" visible ="false">
                <div class="row"  runat="server" style="padding-top:20px;padding-bottom:0px;margin-bottom:0px;">
                    <div class="col-md-12">
                            <div class="form-group">
                            <label>Select the Functions that the Super User will be allowed to access & click the 'Add' button:</label>
                        </div>
                    </div>
                </div>

                <div class="row" style="padding-top:0px;margin-top:0px;">
                    <div class="col-md-2">
                        <table class="table">
    
                        <tbody>
                            <asp:Repeater ID="superUserFunctionsInsert" runat="server">
                                <ItemTemplate>
                                    <tr>
                                        <td><input type="checkbox" id="FunctionInsert" runat="server" checked='<%# Eval("IsChecked") %>'></label></td>
                                        <td ><label for="Function" id="FunctionNameInsert" runat="server"><%# Eval("Function") %></label></td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </tbody>
                    </table>
                    </div>
                </div>

                <div class="row" runat="server" id="AddFinalRow">
                    <div class="col-md-2">
                        <div class="form-group">
                            <asp:Button ID="btnAddFinal" runat="server" Text="Add" class="btn btn-default" 
                                CommandName="AddFinal"
                                CommandArgument="None"
                                OnCommand="CommandBtn_Click"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading"><b>Delete</b> or <b>Amend</b> an <i>existing</i> Super User.</div>
        <div class="panel-body">

            <div class="row"  runat="server">
                <div class="col-md-12">
                        <div class="form-group">
                        <label for="superUserDropDown">Select a 'Super User' to Delete/Amend & click the corresponding 'action' button.</label>
                        <asp:DropDownList AutoPostBack="true" ID="superUserDropDown" runat="server" CssClass="form-control" />
                    </div>
                </div>
            </div>

            <div class="row" runat="server" id="InitButtonRow">
                 <div class="col-md-2">
                    <div class="form-group">
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" class="btn btn-default" 
                            CommandName="Delete"
                            CommandArgument="None"
                            OnCommand="CommandBtn_Click"/>
                    </div>
                </div>
                    <div class="col-md-2">
                    <div class="form-group">
                        <asp:Button ID="btnAmendInitial" runat="server" Text="Amend" class="btn btn-default" 
                            CommandName="AmendInitial"
                            CommandArgument="None"
                            OnCommand="CommandBtn_Click"/>
                    </div>
                </div>
            </div>

            <div id="amendFunctionRow" runat="server" visible ="false">
                <div class="row"  >
                    <div class="col-md-2">
                        <table class="table">
                            <tbody>
                                <asp:Repeater ID="superUserFunctionsAmend" runat="server">
                                    <ItemTemplate>
                                        <tr>
                                            <td><input type="checkbox" id="FunctionAmend" runat="server" checked='<%# Eval("IsChecked") %>'></label></td>
                                            <td ><label for="Function" id="FunctionNameAmend" runat="server"><%# Eval("Function") %></label></td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="row" >
                    <div class="col-md-2">
                        <div class="form-group">
                            <asp:Button ID="btnAmendFinal" runat="server" Text="Amend" class="btn btn-default" 
                                CommandName="AmendFinal"
                                CommandArgument="None"
                                OnCommand="CommandBtn_Click"/>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <asp:Button ID="btnReset" runat="server" Text="Reset" class="btn btn-default" 
                                CommandName="Reset"
                                CommandArgument="None"
                                OnCommand="CommandBtn_Click"/>
                        </div>
                    </div>  
                </div>

                </div>
            </div>
        </div>
</asp:Content>
﻿<%@ Page Title="" Language="C#" MasterPageFile="Site.Master" AutoEventWireup="true" CodeBehind="Users.aspx.cs" Inherits="Appraisals.Admin.UsersWebForm" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="well well-sm" style="text-align:center">
        <h1 id="objTypeTitle" runat="server">Maintain Administrative Users</h1>
    </div>

    <div id="messages" class="alert alert-success alert-dismissible" runat="server" visible="false">
    </div>
    <div id="errors" class="alert alert-danger alert-dismissible" runat="server" visible="false">
    </div>
    
    <div class="panel panel-default">
        <div class="panel-heading">Add or Delete an Administrator.</div>
        <div class="panel-body">

 
        <div class="row " >
                <div class="col-md-4">
                    <table class="table">
                    <thead>
                        <tr>
                            <th>Usernames of current Administrators</th>
                        </tr>
                    </thead>
                    <tbody>
                        <asp:Repeater ID="administrators" runat="server">
                            <ItemTemplate>
                                <tr>
                                    <td><asp:TextBox  class="form-control"  style="border:none;" ReadOnly="true" ID="Username" runat="server" Text='<%# Eval("Username") %>' /></td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="row"  runat="server">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="username">'Username' of the person to be added/deleted</label>
                    <input type="text"  class="form-control" id="username" runat="server">
                </div>
            </div>

        </div>

        <div class="row" runat="server">
                 <div class="col-md-2">
                    <div class="form-group">
                        <asp:Button ID="btnAdd" runat="server" Text="Add" class="btn btn-default" 
                            CommandName="Add"
                            CommandArgument="None"
                            OnCommand="CommandBtn_Click"/>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" class="btn btn-default" 
                            CommandName="Delete"
                            CommandArgument="None"
                            OnCommand="CommandBtn_Click"/>
                    </div>
                </div>
            </div>
        </div>
</asp:Content>

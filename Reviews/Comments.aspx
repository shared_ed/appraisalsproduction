﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Reviews/ReviewsMaster.Master" AutoEventWireup="true" CodeBehind="Comments.aspx.cs" Inherits="Appraisals.Reviews.WebForm3" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="well well-sm" style="text-align:center">
        <h1 id="objTypeTitle" runat="server">Employee and Manager Comments</h1>
    </div>

    <div id="messages" class="alert alert-success alert-dismissible" runat="server" visible="false">
    </div>
    <div id="errors" class="alert alert-danger alert-dismissible" runat="server" visible="false">
    </div>
    
     <div class="row">
        <div class="col-md-12">
            <h2  id="objSelected" runat="server">Comments</h2>
       </div>
    </div>

    
        <div class="panel panel-default">
            <div class="panel-heading">Comment Details</div>
            <div class="panel-body">

               <div class="row" id="employeeOpportunityRow"  runat="server">
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="checkbox">
                             <%--   <label><input id="employeeOpportunity" type="checkbox" value="" runat="server">Has the employee had the opportunity to comment?</label>--%>
                                <label for="employeeOpportunity"><b>Has the employee had the opportunity to comment?</b></label>
                                <asp:CheckBox id="employeeOpportunity" runat="server"
                                    AutoPostBack="False"
                                    TextAlign="Right"/>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row" id="employeeAppraisalCommentRow"  runat="server">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="employeeAppraisalComment">Employee's comments on Appraisal</label>
                            <textarea class="form-control" style="min-width: 86%" rows="5" id="employeeAppraisalComment" runat="server"></textarea>
                        </div>
                    </div>
                </div>

                <div class="row" id="managerCommentsRow"  runat="server">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="managerComments">Manager's comments</label>
                            <textarea class="form-control" style="min-width: 86%" rows="5" id="managerComments" runat="server"></textarea>
                        </div>
                    </div>
                </div>

                <div id="endOfYearSection" visible="false" runat="server">

                    <div class="row" id="employeePerformanceCommentRow"  runat="server">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="employeePerformanceComment">Employee's comments on Performance</label>
                                <textarea class="form-control" style="min-width: 86%" rows="5" id="employeePerformanceComment" runat="server"></textarea>
                            </div>
                        </div>
                    </div>

                    <div id="academicKPIsection" visible="false" runat="server">
                        <div class="row">
                            <div class="col-md-12">
                                <label for="tblAcademicKPI">KPIs to consider when determining TLA Rating:</label>
                                <table id= "tblAcademicKPI" class="table-bordered" >
                                    <thead>
                                        <tr>
                                            <th>KPI</th>
                                            <th>Target %</th>
                                            <th>Actual %</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <asp:Repeater ID="academicKPI" runat="server">
                                            <ItemTemplate>
                                                <tr>
                                                    <td><asp:TextBox ReadOnly="true"  BorderStyle="None" ID="title" runat="server" Text='<%# Eval("Title") %>' /></td>
                                                    <td><asp:TextBox ReadOnly="true" BorderStyle="None" Columns="3" ID="target" runat="server" Text='<%# Eval("TargetValue") %>' /></td>
                                                    <td><asp:TextBox ReadOnly="true" BorderStyle="None" Columns="3"  ID="actual" runat="server" Text='<%# Eval("Actual") %>' /></td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </tbody>
                                </table>
                            </div>

                        </div>
       
                        <div class="row" id="TLArow"  runat="server" style="padding-top:15px;">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="TlaDropDownList">TLA Rating</label>
                                    <asp:DropDownList ID="TlaDropDownList" runat="server" CssClass="form-control"/>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row" id="ratingRow"  runat="server">
                        <div class="col-md-12">
                             <div class="form-group">
                                <label for="ratingsDropDownList">Overall Rating</label>
                                <asp:DropDownList ID="ratingsDropDownList" runat="server" CssClass="form-control"/>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="row" id="reviewCompleteRow"  runat="server">
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="checkbox">
                               <%-- <label><input id="reviewComplete" type="checkbox" value="" runat="server">Is the review complete?</label>--%>
                                <label for="reviewComplete"><b>Is the review complete?</b></label>
                                <asp:CheckBox id="reviewComplete" runat="server"
                                    AutoPostBack="False"
                                    TextAlign="Right"/>
                            </div>
                        </div>
                    </div>
                </div>

                <button type="submit" class="btn btn-default">Save</button>
            </div>
        </div>

        <asp:HiddenField ID="commentID" runat="server" />
        <asp:HiddenField ID="mgrUserName" runat="server" />

     <script type="text/javascript">

         $(document).ready(function () { //for initial doc load checks etc
         });

         function catchSubmit() //for pre-post checks
         {
             return true;
         }

        // function CellAction(tdControl) {
        //    // //whitewash objectives table i.e. make everything 'unselected'
        //    // $('#MainContent_ObjectivesTable td').each(function (i, cell) {
        //    //     var cell = $(cell);
        //    //     $(cell).css('background-color', 'white');  
        //    // });
        //    // //and now highlight selected row
        //    //$(tdControl).css('background-color', 'orange');  

        //     try {
        //         document.getElementById("MainContent_messages").style.display = 'none';
        //     } catch (err){};
        //     try {
        //         document.getElementById("MainContent_errors").style.display = 'none';
        //     } catch (err) { };

        //    $("#MainContent_reviewDate").val($(tdControl).find("input[type='hidden']:eq(0)").val());
        //    $("#MainContent_RatingsDropDownList").val($(tdControl).find("input[type='hidden']:eq(1)").val());
        //    $("#MainContent_actual").val($(tdControl).find("input[type='hidden']:eq(2)").val());
        //    $("#MainContent_comment").text($(tdControl).find("input[type='hidden']:eq(3)").val());
        //    $("#MainContent_objSelected").html($(tdControl).find("input[type='hidden']:eq(4)").val());
        //    $("#MainContent_ranking").val($(tdControl).find("input[type='hidden']:eq(5)").val());
        //    $("#MainContent_objectiveID").val($(tdControl).find("input[type='hidden']:eq(6)").val());
        //    $("#MainContent_reviewPeriodID").val($(tdControl).find("input[type='hidden']:eq(7)").val());
        //    $("#MainContent_target").val($(tdControl).find("input[type='hidden']:eq(8)").val());
        //}

    </script>
    
   

</asp:Content>


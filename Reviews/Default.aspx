﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Reviews/ReviewsMaster.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Appraisals.Reviews.WebForm1" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">



    <div class="well well-sm" style="text-align:center">
        <h1 id="objTypeTitle" runat="server"></h1>
    </div>

    <div id="messages" class="alert alert-success alert-dismissible" runat="server" visible="false">
    </div>
    <div id="errors" class="alert alert-danger alert-dismissible" runat="server" visible="false">
    </div>

    <div class="row">
        <div class="col-md-12">
            <h2 id="objTypeSummary" runat="server">Objective Summary</h2>
            <p>(Please click an objective to see related Review details)</p>
             <div id="div2" style="height: inherit; overflow: auto; border:1px solid black;">
                <asp:PlaceHolder ID="PlaceHolderObjectiveSummary" runat="server"></asp:PlaceHolder>
            </div>
        </div>
    </div>
    
     <div class="row">
        <div class="col-md-12">
            <h2  id="objSelected" runat="server">Review of selected objective</h2>
       </div>
    </div>

    
        <div class="panel panel-default">
            <div class="panel-heading">Review Details</div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                    
                        <div class="form-group">
                            <label for="reviewDate">Review Date <i>(format: dd/mm/yyyy)</i></label>
                            <input type="text" class="form-control" id="reviewDate" runat="server">
                        </div>
                    </div>
                </div>

                <div class="row" id="ratingRow" style="display:none;" runat="server">
                    <div class="col-md-12">
                         <div class="form-group">
                            <label for="RatingsDropDownList">Rating</label>
                            <asp:DropDownList ID="RatingsDropDownList" runat="server" CssClass="form-control"/>
                        </div>
                    </div>
                </div>

                <div class="row" id="performanceRow" style="display:none;" runat="server">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="actual">Actual percentage<i>(format: number between 0 and 100)</i></label>
                            <input type="text" class="form-control" id="actual"  runat="server">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="target">Target percentage</label>
                            <input type="text" class="form-control" id="target"  runat="server" readonly>
                        </div>
                    </div>
                </div>

                <div class="row" id="commentRow" style="display:none;" runat="server">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="comment">Comment</label>
                            <textarea class="form-control" style="min-width: 86%" rows="5" id="comment" runat="server"></textarea>
                        </div>
                    </div>
                </div>

                <button type="submit" class="btn btn-default">Save</button>
            </div>
    </div>

        <div class="panel panel-default" id="midYearSummary" runat="server" visible="false">
            <div class="panel-heading">Mid-Year Assessment Summary</div>
             <div class="panel-body">   
                <div class="row">
                    <div class="col-md-12">
                      <table class="table">
                        <thead>
                            <tr>
                                <th>Objective</th>
                                <th>Review date</th>
                                <th>Rating</th>
                                <th>Actual %</th>
                                <th>Comments</th>
                            </tr>
                        </thead>
                        <tbody>
                            <asp:Repeater ID="midTermSummary" runat="server">
                                <ItemTemplate>
                                    <tr>
                                        <td><asp:TextBox ReadOnly="true" Columns="200" BorderStyle="None" ID="MTobjective" runat="server" Text='<%# Eval("Title") %>' /></td>
                                        <td><asp:TextBox ReadOnly="true" BorderStyle="None"  ID="MTreviewDate" runat="server" Text='<%# Eval("ReviewDate") %>' /></td>
                                        <td><asp:TextBox ReadOnly="true" BorderStyle="None" Columns="2" ID="MTRating" runat="server" Text='<%# Eval("Rating") %>' /></td>
                                        <td><asp:TextBox ReadOnly="true" BorderStyle="None" Columns="3"  ID="MTActual" runat="server" Text='<%# Eval("Actual") %>' /></td>
                                        <td><asp:TextBox ReadOnly="true" TextMode="multiline" Columns="200" Rows="5" BorderStyle="None"  ID="MTComments" runat="server" Text='<%# Eval("Comment") %>' /></td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

        <asp:HiddenField ID="ranking" runat="server" />
        <asp:HiddenField ID="objectiveID" runat="server" />
        <asp:HiddenField ID="reviewPeriodID" runat="server" />
        <asp:HiddenField ID="rowIndex" runat="server" />

     <script type="text/javascript">

         $(document).ready(function () { //for initial doc load checks etc
         });

         function catchSubmit() //for pre-post checks
         {
             return true;
         }

         function CellAction(tdControl) {
           

             try
             {
                 document.getElementById("MainContent_messages").style.display = 'none';
             } catch (err){};
             try
             {
                 document.getElementById("MainContent_errors").style.display = 'none';
             }
             catch (err) { };

            $("#MainContent_reviewDate").val($(tdControl).find("input[type='hidden']:eq(0)").val());
            $("#MainContent_RatingsDropDownList").val($(tdControl).find("input[type='hidden']:eq(1)").val());
            $("#MainContent_actual").val($(tdControl).find("input[type='hidden']:eq(2)").val());
            $("#MainContent_comment").text($(tdControl).find("input[type='hidden']:eq(3)").val());
            $("#MainContent_objSelected").html($(tdControl).find("input[type='hidden']:eq(4)").val());
            $("#MainContent_ranking").val($(tdControl).find("input[type='hidden']:eq(5)").val());
            $("#MainContent_objectiveID").val($(tdControl).find("input[type='hidden']:eq(6)").val());
            $("#MainContent_reviewPeriodID").val($(tdControl).find("input[type='hidden']:eq(7)").val());
            $("#MainContent_target").val($(tdControl).find("input[type='hidden']:eq(8)").val());
            $("#MainContent_rowIndex").val($(tdControl).find("input[type='hidden']:eq(9)").val());

            //whitewash objectives table i.e. make everything 'unselected'
             $('#MainContent_ObjectivesTable td').each(function (i, cell) {
                 var cell = $(cell);
                 $(cell).css('background-color', 'white');  
             });
            //and now highlight selected row
            $(tdControl).css('background-color', 'orange');  
        }

    </script>
    
   

</asp:Content>


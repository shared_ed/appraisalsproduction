﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Reviews/ReviewsMaster.Master" AutoEventWireup="true" CodeBehind="NoData.aspx.cs" Inherits="Appraisals.Reviews.WebForm2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="container">
      <div class="jumbotron">
        <h2>There is no data available to support your request.</h2>      
        <p>Please check the parameters being passed to the HR Appraisals application.</p>
      </div>
    </div>
</asp:Content>

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Reviews/ReviewsMaster.Master" AutoEventWireup="true" CodeBehind="NotAuthorised.aspx.cs" Inherits="Appraisals.Reviews.WebForm2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="container">
      <div class="jumbotron">
        <h2>Authorisation Failure</h2>      
        <p>You are not currently authorised to maintain appraisals for this member of staff.</p>
        <p>To acquire the necessary permissions, please contact HR and provide them with <i>your</i> Username and the Names, Staffcodes & Usernames of the staff members for whom you complete appraisals.</p>
        <p>Thank you.</p>
      </div>
    </div>
</asp:Content>

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Reviews/ReviewsMaster.Master" AutoEventWireup="true" CodeBehind="NotValid.aspx.cs" Inherits="Appraisals.Reviews.WebForm2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="container">
      <div class="jumbotron">
        <h2>Request Validation Failure</h2>      
        <p>You have submitted an invalid request to the HR Appraisals application.</p>
        <p>Please review the URL you are submitting.</p>
        <p>Thank you.</p>
      </div>
    </div>
</asp:Content>
